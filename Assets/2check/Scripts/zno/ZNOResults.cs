﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ZNOResults : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI ukrainianScoreText;
	[SerializeField] TextMeshProUGUI mathsScoreText;
	[SerializeField] TextMeshProUGUI thirdScoreText;
	[Space]
	[SerializeField] Image thirdImage;
	[Space]
	[SerializeField] Sprite englishSprite;
	[SerializeField] Sprite physicsSprite;

	public static int UkrainianScore = -1;
	public static int MathsScore = -1;
	public static int ThirdScore = -1;

	void Start()
	{
		SetScoreTexts();

		SetThirdImage();
	}

	void SetScoreTexts()
	{
		ukrainianScoreText.text = UkrainianScore == -1 ? "---" : UkrainianScore.ToString();
		mathsScoreText.text = MathsScore == -1 ? "---" : MathsScore.ToString();
		thirdScoreText.text = ThirdScore == -1 ? "---" : ThirdScore.ToString();
	}

	void SetThirdImage()
	{
		thirdImage.sprite = ZNOMenu.EnglishSelected ? englishSprite : physicsSprite;
	}

	public static void CalculateScore(int[] answers, Questions q)
	{
		int score = 100;

		for (int i = 0; i < 10; i++)
		{
			if (answers[i] == q[i].answerId)
				score += 10;
		}

		switch (ZNOMenu.CurLevelId)
		{
			case 0:
				UkrainianScore = score;
				break;
			case 1:
				MathsScore = score;
				break;
			case 2:
			case 3:
				ThirdScore = score;
				break;
		}
	}
}
