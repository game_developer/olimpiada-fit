﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ZNOMenu : MonoBehaviour
{
	[SerializeField] Image registrationBlock;
	[SerializeField] Button registrationButton;

	public static int CurLevelId { get; set; }
	public static bool EnglishSelected { get; set; }

	public static bool[] SubjectsCompleteStatus = new bool[3];
    public static bool ZnoChoosen = false;

	void Start()
	{
        if(!ZnoChoosen)
        {
			SceneManager.LoadScene("choice_zno");
        }
           
        CheckRegistration();

        if (!EnglishSelected)
        {
            GameObject.Find("Third Subject").GetComponentInChildren<TextMeshProUGUI>().text = "Фізика";
            GameObject.Find("SubjectImagePhysic").GetComponent<Image>().enabled = true;
        }
        else
        {
            GameObject.Find("Third Subject").GetComponentInChildren<TextMeshProUGUI>().text = "Англійська мова";
            GameObject.Find("SubjectImageEnglish").GetComponent<Image>().enabled = true;
        }
	}

	void CheckRegistration()
	{
		if(SubjectsCompleteStatus.All(s => s))
		{
			registrationBlock.enabled = false;
			registrationButton.enabled = true;
		}
	}

	public static void OnSubjectCompleted()
	{
		SubjectsCompleteStatus[Mathf.Clamp(CurLevelId, 0, 2)] = true;
	}

	public void OnWheelButtonClick(int buttonID)
	{
		if (buttonID == 3)
			buttonID = 4;

		else if (buttonID == 2 && !EnglishSelected)
			buttonID = 3;

		CurLevelId = buttonID;

        if (buttonID <= 3)
            SceneManager.LoadScene("tests_zno");

        else
            SceneManager.LoadScene("Registration");
	}
}
