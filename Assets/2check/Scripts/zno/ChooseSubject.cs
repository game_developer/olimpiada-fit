﻿using UnityEngine;

public class ChooseSubject : MonoBehaviour
{
	public static bool physics;

	public void Click()
	{
        ZNOMenu.ZnoChoosen = true;
        if (gameObject.name == "English")
		{
			physics = false;
			ZNOMenu.EnglishSelected = true;
		}

		UnityEngine.SceneManagement.SceneManager.LoadScene("menu_zno");
	}
}
