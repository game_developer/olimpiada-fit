﻿using UnityEngine;

public class NextStepManager : MonoBehaviour
{
	public void Submit()
	{
		HelpZNO_2.completed = true;
        if(StepMenu.LastUnlocked < 2)
            StepMenu.LastUnlocked = 2;
        UnityEngine.SceneManagement.SceneManager.LoadScene("menu_zno");
	}

	public void NextStep()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
	}
}
