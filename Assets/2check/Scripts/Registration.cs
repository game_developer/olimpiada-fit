﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Registration : MonoBehaviour
{
	public static Dictionary<string, bool> isValid = new Dictionary<string, bool>
	{
		{"Name",false},
		{"School",false},
		{"PhoneNumber",false},
		{"CertificateNumber",false},
		{"CertificateYear", true },
		{"Pin",false},
		{"SchoolCertificateNumber", false },
		{"SchoolMark", false },
		{"Understand", false },
        {"CalculateMark", false }
	};

    void Start()
    {
        if (ChooseSubject.physics)
        {
            GameObject.Find("ZNO_3").GetComponentInChildren<Text>().text = "Фізика:";
        }
        else
        {
            GameObject.Find("ZNO_3").GetComponentInChildren<Text>().text = "Англійська мова:";
        }
        GameObject.Find("ZNO_1").GetComponent<UnityEngine.UI.InputField>().text = Convert.ToString(ZNOResults.UkrainianScore);
        GameObject.Find("ZNO_2").GetComponent<UnityEngine.UI.InputField>().text = Convert.ToString(ZNOResults.MathsScore);
        GameObject.Find("ZNO_3").GetComponent<UnityEngine.UI.InputField>().text = Convert.ToString(ZNOResults.ThirdScore);
        Form.CheckForUpdate();
    }
}
