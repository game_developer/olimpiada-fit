﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Form : MonoBehaviour {
    [SerializeField] Text hint;
    bool IsEmpty()
    {
        if(gameObject.GetComponentInChildren<Text>().text.Length == 0)
        {
            Registration.isValid[gameObject.name] = false;
            return true;
        }
        Registration.isValid[gameObject.name] = true;
        return false;
    }
    //Hint become empty
    public void OnChange()
    {
        GameObject.Find("Canvas").GetComponent<PauseWindow>().enabled = false;
        hint.text = "";
    }
    public void Validation()
    {
        if (IsEmpty())
        {
            hint.text = "Поле повинно бути заповненим";
        }
        CheckForUpdate();
    }
    //Check field with numbers for correct value
    public void NumberValidation()
    {
        if(IsEmpty())
            hint.text = "Поле повинно бути заповненим";
        int number = 0;
        switch (gameObject.name)
        {
            case "PhoneNumber" :
                number = 10;
                break;
            case "CertificateNumber":
                number = 7;
                break;
            case "Pin":
                number = 4;
                break;
            case "SchoolCertificateNumber":
                number = 6;
                break;
        }
        if (gameObject.GetComponentInChildren<Text>().text.Length < number && gameObject.GetComponentInChildren<Text>().text.Length > 0)
        {
            Registration.isValid[gameObject.name] = false;
            hint.text = "Поле має містити " + number + " цифр";
        }
        CheckForUpdate();
    }
    //Check SchoolMark field with numbers for correct value
    public void MarkValidation()
    {
        if(IsEmpty())
            hint.text = "Поле повинно бути заповненим";
        else if (Convert.ToDouble(gameObject.GetComponentInChildren<Text>().text) > 12 || Convert.ToDouble(gameObject.GetComponentInChildren<Text>().text) < 0 
            || gameObject.GetComponentInChildren<Text>().text[0] == '0' || gameObject.GetComponentInChildren<Text>().text[0] =='-')
        {
            Registration.isValid[gameObject.name] = false;
            hint.text = "Некоректна оцінка";
        }
        CheckForUpdate();
    }
    // First part of CertificateNumber must be in uppercase
    public void OnSchoolCertificateNumberChange()
    {
        GameObject.Find("Canvas").GetComponent<PauseWindow>().enabled = false;
        string text = GameObject.Find(gameObject.name).GetComponent<UnityEngine.UI.InputField>().text;
        for (int i = 48; i < 58; i++)
        {
            if (text.Length > 0 && text[text.Length - 1] == i)
            {
                text = text.Remove(text.Length - 1);
            }
        }
        GameObject.Find(gameObject.name).GetComponent<UnityEngine.UI.InputField>().text = text.ToUpperInvariant();
    }
    // Check email field for correct format
    public void EmailValidate()
    {
        string text = gameObject.GetComponentInChildren<Text>().text;
        if (IsEmpty())
        {
            hint.text = "Поле повинно бути заповненим";
        }
        else if (text.Contains("@"))
        {
            if (text.Contains(".") && text.LastIndexOf('.') > text.IndexOf('@')+1 && text.LastIndexOf('.') < text.Length-1)
            {
                hint.text = "";
            }
            else
            {
                hint.text = "Email має невірний формат";
                Registration.isValid[gameObject.name] = false;
            }
        }
        else
        {
            hint.text = "Email має невірний формат";
            Registration.isValid[gameObject.name] = false;
        }
        CheckForUpdate();
    }
    public void Understand()
    {
        Registration.isValid[gameObject.name] = gameObject.GetComponent<Toggle>().isOn;
        CheckForUpdate();
    }
    //Check form for valid data in even field
    public static void CheckForUpdate()
    {
        GameObject.Find("Canvas").GetComponent<PauseWindow>().enabled = true;
        bool valid = true;
        foreach (var pare in Registration.isValid)
        {
            valid &= pare.Value;
        }
        if (valid)
            GameObject.Find("Submit").GetComponent<Button>().enabled = true;
        else
            GameObject.Find("Submit").GetComponent<Button>().enabled = false;
    }
    public void ShowZNO()
    {
        GameObject.Find("score").GetComponent<UnityEngine.UI.InputField>().text = Convert.ToString(
        (double)ZNOResults.UkrainianScore * 0.2 + ZNOResults.MathsScore * 0.6 + ZNOResults.ThirdScore * 0.2);
    }
    public void CalculateMarks()
    {
        Registration.isValid["CalculateMark"] = true;
        CheckForUpdate();
    }
}
