﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HelpZNO_2 : MonoBehaviour
{
    [SerializeField] CanvasGroup ukrainianTip;
    [SerializeField] GameObject[] helpers;
    [SerializeField] GameObject mainHelp;

    public static bool completed = false;
	static bool FirstTime = true;

	int tipsShown = 1;

	void Start()
    {
        completed = StepMenu.LastUnlocked >= 2;
        if (completed)
        {
            mainHelp.SetActive(true);
        }
        else if (FirstTime)
        {
            helpers[0].SetActive(true);
            helpers[1].SetActive(true);
        }
    }

    public void ShowNextTip()
    {
        FirstTime = false;
        if (tipsShown == helpers.Length - 1)
        {
            helpers[0].SetActive(false);
        }
        else
        {
            helpers[tipsShown].SetActive(false);
            helpers[tipsShown + 1].SetActive(true);
        }

        if (tipsShown == 1)
            ukrainianTip.GetComponentInParent<EventTrigger>().OnPointerEnter(null);

        if (tipsShown == 2)
            ukrainianTip.GetComponentInParent<EventTrigger>().OnPointerExit(null);

            ++tipsShown;
    }
}
