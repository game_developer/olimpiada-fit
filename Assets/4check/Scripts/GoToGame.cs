﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class GoToGame : MonoBehaviour
{
	public static bool[] Clicked = new bool[3];

	public static bool YaTebeHaknyvCompleted = false;
    [SerializeField] GameObject afterLast;
    [SerializeField] string videoName;
    [SerializeField] int timeInSeconds;// Время которое идет видео
    [SerializeField] GameObject tip;
    void Start()
    {
        if (YaTebeHaknyvCompleted)
        {
            afterLast.SetActive(true);
            tip.SetActive(false);
        }
    }
	void Update()
	{
		if (Clicked[0] && Clicked[1] && Clicked[2])
			GameObject.Find("Game").GetComponent<Button>().enabled = true;
	}
    public void ShowTip()
    {
        tip.SetActive(true);
    }
	public void Game()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("kill_the_bugs_game");
    }
    public void SartVideo()
    {
		Servak.Instance.SaveWithoutExiting();

        GameObject camera = GameObject.Find("Main Camera");

        var videoPlayer = camera.GetComponent<VideoPlayer>();
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        videoPlayer.targetCameraAlpha = 1F;
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, videoName);
        videoPlayer.frame = 0;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
        videoPlayer.isLooping = false;
        videoPlayer.loopPointReached += EndReached;
        videoPlayer.Play();
        GoToProfile();
    }
    void EndReached(VideoPlayer vp)
    {
        vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    }
    void GoToProfile()
    {
        // url - профиль пользователя 
        Application.OpenURL("https://cyber-game.herokuapp.com/congratulations");
    }
}
