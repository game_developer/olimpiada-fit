﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

public class VideoVar : MonoBehaviour {
    public int indexButton;
    [SerializeField] string videoName; // Путь к видео
    [SerializeField] int timeInSeconds; // Время которое идет видео
    [SerializeField] AudioClip audioClip;
    [SerializeField] GameObject tip;
    static bool FirstClick = true;
    void Update()
    {
        if (Input.GetKey(KeyCode.Return) && !GoToGame.YaTebeHaknyvCompleted)
        {
            GameObject.Find("Main Camera").GetComponent<VideoPlayer>().Stop();
            GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
            CancelInvoke();


        }
    }
    public void Click()
    {
        GoToGame.Clicked[indexButton] = true;
        if (FirstClick)
        {
            ShowTip();
            FirstClick = false;
            return;
        }
        tip.SetActive(false);
        GameObject camera = GameObject.Find("Main Camera");

        var videoPlayer = camera.GetComponent<VideoPlayer>();
        var audioPlayer = camera.GetComponent<AudioSource>();
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
		videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, videoName);
        videoPlayer.targetCameraAlpha = 1F;
        audioPlayer.clip = audioClip;
        videoPlayer.frame = 0;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
        videoPlayer.isLooping = false;
        videoPlayer.loopPointReached += EndReached;
        audioPlayer.Play();
        videoPlayer.Play();
        Invoke("StopVideo", timeInSeconds);
        gameObject.GetComponent<Button>().enabled = false;
    }

	public void ShowTip()
    {
        tip.SetActive(true);
        tip.GetComponent<Button>().onClick.AddListener(Click);
    }

	void EndReached(VideoPlayer vp)
    {
        vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    }
    void StopVideo()
    {
        GameObject.Find("Main Camera").GetComponent<VideoPlayer>().Stop();
        GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
    }
}
