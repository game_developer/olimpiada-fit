﻿using UnityEngine;
using UnityEngine.UI;


public class StructButton : MonoBehaviour {
    [SerializeField] GameObject structInfo;
    public int index;
	public void ShowInfo()
    {
        GoToGame.Clicked[index] = true;
        structInfo.SetActive(true);
        gameObject.GetComponent<Button>().enabled = false;
    }

	public void HideInfo()
    {
        
        structInfo.SetActive(false);
    }

}
