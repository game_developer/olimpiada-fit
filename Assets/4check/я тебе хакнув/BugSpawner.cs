﻿using System.Collections;
using TMPro;
using UnityEngine;

public class BugSpawner : MonoBehaviour 
{
	[SerializeField] Sprite[] bugImages;
	[SerializeField] GameObject bugPrefab;
	[SerializeField] TextMeshPro BugsDestroyedText;
	[SerializeField] GameObject losePanel;
	[Space]
	public AudioSource smash;
	[SerializeField] AudioSource proebal;

	/// <summary>
	/// Controls when the game ends.
	/// If player reaches this point then it all ends up.
	/// As far as I tested optimal values is 40.
	/// Less than 40 is too easy.	
	/// </summary>
	int bugsToWin = 30;

	Coroutine spawnBugsCoroutine;

	int _bugsKilled;
	public int BugsKilled
	{
		get { return _bugsKilled; }
		set
		{
			_bugsKilled = value;

			BugsDestroyedText.text = "багів знищено: " + value;

			if (_bugsKilled == bugsToWin)
			{
				GoToGame.YaTebeHaknyvCompleted = true;
                UnityEngine.SceneManagement.SceneManager.LoadScene("4checkpoint");
            }
		}
	}

	int bugsSpawned = 0;

	void Start()
	{
		BugsDestroyedText = FindObjectOfType<TextMeshPro>();

		spawnBugsCoroutine = StartCoroutine(SpawnBugs());
	}

	IEnumerator SpawnBugs()
	{
		while(bugsSpawned < bugsToWin)
		{
			yield return new WaitForSeconds(Random.Range(0.65f, 0.8f));

			SpawnBug();

			bugsSpawned++;
		}
	}

	public void Lose()
	{
		StopCoroutine(spawnBugsCoroutine);

		losePanel.SetActive(true);

		foreach(Transform t in transform)
			t.GetComponent<Bug>().Move = false;

		proebal.Play();
	}

	void SpawnBug()
	{
		GameObject bug = Instantiate(bugPrefab, this.transform);
		bug.GetComponent<SpriteRenderer>().sprite = bugImages[Random.Range(0, bugImages.Length)];
	}

	public void OnTryAgainClicked()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
	}
}
