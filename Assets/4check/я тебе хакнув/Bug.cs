﻿using TMPro;
using UnityEngine;

public class Bug : MonoBehaviour
{
	BugSpawner bugSpawner;

	public bool Move = true;

	float moveSpeed = 4;
	bool moveRight;
	const int OFFSET = 11;

	void Start()
	{
		bugSpawner = transform.parent.GetComponent<BugSpawner>();

		moveRight = Random.Range(0, 2) == 0 ? false : true;

		transform.position = new Vector2(moveRight ? -OFFSET : OFFSET, -1);
		transform.rotation = Quaternion.Euler(0f, 0f, moveRight ? -90f : 90f);
	}

	void FixedUpdate()
	{
		if(Move)
			transform.Translate(Vector2.up * Time.fixedDeltaTime * moveSpeed, Space.Self);
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		FindObjectOfType<BugSpawner>().Lose();
	}

	void OnMouseDown()
	{
		Destroy(gameObject);

		Animator smileBubble = GameObject.Find("Smile Bubble").GetComponent<Animator>();
		smileBubble.GetComponentInChildren<TextMeshProUGUI>().text = "<sprite=" + Random.Range(0, 11) + ">";
		smileBubble.SetTrigger("BugKilled");

		GameObject.Find("Laptop").GetComponent<Animator>().SetTrigger("BugKilled");

		bugSpawner.BugsKilled++;

		bugSpawner.smash.Play();
	}
}