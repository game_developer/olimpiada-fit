﻿using UnityEngine;

public class PauseWindow : MonoBehaviour
{
	[SerializeField] GameObject pauseImage;
	void Update()
	{
		if (Input.GetKey("p"))
			Open();
	}

	public void Close()
	{
		pauseImage.SetActive(false);
	}

	public void Open()
	{
		pauseImage.SetActive(true);
	}

	public void Exit()
	{
		Checkpoint.LastMiniGamePlayed = -1;
		Checkpoint.LastGameEnd = LastGameEnd.None;

        LoadMenu();
	}

	public void Quit()
	{
		Servak.Instance.SaveAndExit();
	}

	void LoadMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
    }
}
