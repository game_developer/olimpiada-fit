﻿using UnityEngine;
using UnityEngine.UI;

public class StepMenu : MonoBehaviour
{
	[SerializeField] string scene;
	[SerializeField] int index;

	public static int LastUnlocked { get; set; }

	void Start()
	{
		if (index > LastUnlocked)
		{
			GetComponent<Button>().enabled = false;
			transform.GetChild(0).GetComponent<Image>().enabled = true;
		}
	}

	public void NextStep()
	{
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }
}
