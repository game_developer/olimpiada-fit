﻿using UnityEngine;

public class LockAnimation : MonoBehaviour
{
    public void On()
    {
        gameObject.GetComponent<Animator>().enabled = true;
        
    }
    public void Exit()
    {
        gameObject.GetComponent<Animator>().enabled = false;
        gameObject.transform.rotation = Quaternion.identity;
    }
}
