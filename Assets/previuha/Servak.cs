﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Servak : MonoBehaviour
{
	public bool Ready { get; private set; }

	#region Singleton
	public static Servak Instance;

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
			Destroy(this.gameObject);

		StartCoroutine(Read());
	}
	#endregion

	IEnumerator Read()
	{
		string url = "https://cyber-api.herokuapp.com/users?uid=" + GetUserId();

		using (WWW www = new WWW(url))
		{
			yield return www;

			SetVariables(www.text);

			print("Ответ пришел");
			print("Текст ввв: " + www.text);
		}

		Ready = true;
	}

	IEnumerator _write(bool exit)
	{
		string url = @"https://cyber-api.herokuapp.com/save";

		WWWForm form = new WWWForm();
		form.headers[@"Content-Type"] = @"application/x-www-form-urlencoded";

		form.AddField(@"uid", GetUserId());
		form.AddField(@"progress", GetCurrentSave());

		using (WWW www = new WWW(url, form))
		{
			yield return www;

			print("GetCurrentSave() = " + GetCurrentSave());
		}

		if (exit)
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBGL
			Application.OpenURL("https://cyber-game.herokuapp.com/home");
#else
			Application.Quit();
#endif
		}
	}

	public void SaveWithoutExiting()
	{
		StartCoroutine(_write(false));
	}

	public void SaveAndExit()
	{
		StartCoroutine(_write(true));
	}

	string GetCurrentSave()
	{
		//0 _ 0 _ 0 _ 0 _ 0
		//1 _ (0 _ 0 _ 0) _ физика _ 100 _ 100 _ 100
		//2 _ физика _ 100 _ 100 _ 100
		//3 _ физика _ 100 _ 100 _ 100 _ 0 _ 0 _ 0 _ (я тебе хакнув  )

		string toSave = StepMenu.LastUnlocked.ToString();

		if (StepMenu.LastUnlocked == 1)
		{
			toSave += "_" + (ZNOMenu.SubjectsCompleteStatus[0] ? "1" : "0");
			toSave += "_" + (ZNOMenu.SubjectsCompleteStatus[1] ? "1" : "0");
			toSave += "_" + (ZNOMenu.SubjectsCompleteStatus[2] ? "1" : "0");
		}

		if (StepMenu.LastUnlocked >= 1)
		{
			if (ZNOMenu.ZnoChoosen == false)
				toSave += "_-1";
			else
				toSave += "_" + (ZNOMenu.EnglishSelected ? "1" : "0");

			toSave += "_" + ZNOResults.UkrainianScore;
			toSave += "_" + ZNOResults.MathsScore;
			toSave += "_" + ZNOResults.ThirdScore;
		}

		if (StepMenu.LastUnlocked == 0)
		{
			for (int i = 0; i < 4; i++)
				toSave += "_" + (Checkpoint.MiniGameWins[i] ? "1" : "0");
		}

		if (StepMenu.LastUnlocked == 3)
		{
			toSave += "_" + (GoToGame.Clicked[0] == true ? "1" : "0");
			toSave += "_" + (GoToGame.Clicked[1] == true ? "1" : "0");
			toSave += "_" + (GoToGame.Clicked[2] == true ? "1" : "0");

			toSave += "_" + (GoToGame.YaTebeHaknyvCompleted == true ? "1" : "0");

		}

		return toSave;
	}

	void SetVariables(string save)
	{
		//0 _ 0 _ 0 _ 0 _ 0
		//1 _ (0 _ 0 _ 0) _ физика _ 100 _ 100 _ 100
		//2 _ физика _ 100 _ 100 _ 100
		//3 _ физика _ 100 _ 100 _ 100 _ 0 _ 0 _ 0 _ (я тебе хакнув)

		string[] formattedSave = save.Split('_');
		int lastUnlocked = Convert.ToInt32(formattedSave[0]);

		StepMenu.LastUnlocked = lastUnlocked;

		if (lastUnlocked == 0)
		{
			//миниигры
			Checkpoint.MiniGameWins[0] = formattedSave[1] == "1" ? true : false;
			Checkpoint.MiniGameWins[1] = formattedSave[2] == "1" ? true : false;
			Checkpoint.MiniGameWins[2] = formattedSave[3] == "1" ? true : false;
			Checkpoint.MiniGameWins[3] = formattedSave[4] == "1" ? true : false;
		}
		else
		{
			//Все игрули тогда пройдены, если считать шо ласт унлокед не 
			//равен единице.
			for (int i = 0; i < 4; i++)
				Checkpoint.MiniGameWins[i] = true;
		}

		if (lastUnlocked == 1)
		{
			//Сеттим комплиты на зно там где надо
			ZNOMenu.SubjectsCompleteStatus[0] = formattedSave[1] == "1" ? true : false;
			ZNOMenu.SubjectsCompleteStatus[1] = formattedSave[2] == "1" ? true : false;
			ZNOMenu.SubjectsCompleteStatus[2] = formattedSave[3] == "1" ? true : false;


			//физика
			if (formattedSave[4] == "-1")
				ZNOMenu.ZnoChoosen = false;

			else
			{
				ZNOMenu.EnglishSelected = formattedSave[4] == "1" ? true : false;
				ZNOMenu.ZnoChoosen = true;
			}

			//скоры
			ZNOResults.UkrainianScore = Convert.ToInt32(formattedSave[5]);
			ZNOResults.MathsScore = Convert.ToInt32(formattedSave[6]);
			ZNOResults.ThirdScore = Convert.ToInt32(formattedSave[7]);
		}

		else if (lastUnlocked > 1)
		{
			//Комплитим все предметы зно
			for (int i = 0; i < 3; i++)
				ZNOMenu.SubjectsCompleteStatus[i] = true;


			ZNOMenu.ZnoChoosen = true;
			ZNOMenu.EnglishSelected = formattedSave[2] == "1" ? true : false;


			//скоры
			ZNOResults.UkrainianScore = Convert.ToInt32(formattedSave[2]);
			ZNOResults.MathsScore = Convert.ToInt32(formattedSave[3]);
			ZNOResults.ThirdScore = Convert.ToInt32(formattedSave[4]);
		}

		if (lastUnlocked == 3)
		{
			//Сеттим проходы видео в 4 этапе
			GoToGame.Clicked[0] = formattedSave[5] == "1" ? true : false;
			GoToGame.Clicked[1] = formattedSave[6] == "1" ? true : false;
			GoToGame.Clicked[2] = formattedSave[7] == "1" ? true : false;

			//Игруля с кул хацкером.
			GoToGame.YaTebeHaknyvCompleted = formattedSave[8] == "1" ? true : false;
		}
	}

	string GetUserId()
	{
#if (UNITY_EDITOR || UNITY_STANDALONE)
		return "gAfdlYrVVYadWPasWgkUxEWgz1I3";
#elif UNITY_WEBGL
		string url = Application.absoluteURL;

		int lastIndex = url.LastIndexOf("=");

		if (lastIndex != -1)
		{
			return url.Substring(lastIndex + 1);
		}

		else
		{
			Application.OpenURL("https://cyber-game.herokuapp.com/home");
			return null;
		}
#else
		return null;
#endif
	}
}