﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Прошу вас, не меняйте ничего а то у меня и так бомбит.
/// А будит еще больше.
/// З.Ы.: Рили не надо.
/// </summary>
public class Previuha : MonoBehaviour
{
	[SerializeField] Transform transformPreviuha;
	[Space]
	[SerializeField] float fadeInTime = .2f;
	[SerializeField] float fadeOutTime = 1;

	CanvasGroup previuha;

	bool fadedIn = false;
	bool beganFadingOut = false;

	void Start()
	{
		previuha = FindObjectOfType<CanvasGroup>();

		StartCoroutine(Fade(@out: false));
		StartCoroutine(MakeBigger());
	}

	//Чекаем все ли готово: Сервак залоадился, ФейдИн прошел.
	void Update()
	{
		if(Servak.Instance.Ready && fadedIn && !beganFadingOut)
		{
			StartCoroutine(Fade(@out: true));

			beganFadingOut = true;
		}
	}

	IEnumerator Fade(bool @out)
	{
		float curTime = 0;
		while (curTime < (@out ? fadeOutTime : fadeInTime))
		{
			curTime += Time.deltaTime;
			previuha.alpha = @out ? 1 - curTime / fadeOutTime : curTime / fadeInTime;

			yield return null;
		}

		if (!@out)
			fadedIn = true;

		if (@out)
			UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
	}

	IEnumerator MakeBigger()
	{
		float initialScale = transformPreviuha.localScale.x;
		float lastSize = initialScale * 1.8f;

		float curTime = 0;
		const float ENDTIME = 15f;
		while(curTime < ENDTIME)
		{
			curTime += Time.deltaTime;
			float scale = Mathf.Lerp(initialScale, lastSize, curTime / ENDTIME);
			transformPreviuha.localScale = new Vector3(scale, scale, 1);

			yield return null;
		}
	}
}
