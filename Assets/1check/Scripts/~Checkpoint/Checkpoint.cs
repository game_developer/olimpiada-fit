﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum LastGameEnd
{
	None,
	Lose,
	Win
}

public class Checkpoint : MonoBehaviour
{
	[SerializeField] AudioSource winSound;
	[SerializeField] AudioSource loseSound;

    [SerializeField] GameObject comp;
    [SerializeField] GameObject buttonsParent;
    [SerializeField] GameObject complete;

	public static bool[] MiniGameWins = new bool[4];
	public static int LastMiniGamePlayed = -1;
	public static LastGameEnd LastGameEnd = LastGameEnd.None;

	void Start()
	{
        if (LastGameEnd == LastGameEnd.Win)
            MiniGameWins[LastMiniGamePlayed] = true;

        bool completed = true;

		for (int i = 0; i < 4; i++)
        {
            if (MiniGameWins[i])
            {
                Transform kid = buttonsParent.transform.GetChild(i);

                kid.GetComponentInChildren<TextMeshProUGUI>().text = "Міні гру\nпройдено";
                kid.GetComponentInChildren<TextMeshProUGUI>().fontSize = 22;
            }
            completed &= MiniGameWins[i];
        }

        if (completed) //&& LastMiniGamePlayed != -1)
        {
            complete.SetActive(true);
            if(StepMenu.LastUnlocked < 1)
			    StepMenu.LastUnlocked = 1;
        }

		if (LastGameEnd == LastGameEnd.None)
			return;

		comp.GetComponentInChildren<Text>().text = LastGameEnd == LastGameEnd.Lose ? "Ви програли. Спробуйте ще! ((" 
			: "Ви виграли! Вітаємо! ))";

		HandleSound();
	}

	void HandleSound()
	{
		if (LastGameEnd == LastGameEnd.Win)
			winSound.Play();

		else if (LastGameEnd == LastGameEnd.Lose)
			loseSound.Play();
	}
}
