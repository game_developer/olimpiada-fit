﻿using UnityEngine;
using UnityEngine.UI;
//TODO: Set up generic tip system
public class Tips : MonoBehaviour
{
	static bool FirstTime = true;
	int tipsShown = 0;
    [SerializeField] GameObject help;
    [SerializeField] GameObject main_help;
    [SerializeField] GameObject start_game;
    [SerializeField] GameObject results;
    [SerializeField] GameObject comp;
    [SerializeField] GameObject rules;
    [SerializeField] GameObject tip;
    [SerializeField] GameObject esc;
    void Start()
	{
		if (FirstTime&& StepMenu.LastUnlocked <1)
		{
            help.SetActive(true);
            main_help.SetActive(true);
		}
	}

	public void ShowNextTip()
	{
        FirstTime = false;
        if (tipsShown == 0)
		{
            main_help.SetActive(false);
            start_game.SetActive(true);
        }

		else if (tipsShown == 1)
		{
            start_game.SetActive(false);
            results.SetActive(true);
            comp.GetComponentInChildren<Text>().text = "Ви виграли! Вітаємо! ))";
        }

		else if (tipsShown == 2)
		{
            results.SetActive(false);
            comp.GetComponentInChildren<Text>().text = "";
            rules.SetActive(true);
            tip.SetActive(true);


        }
        else if (tipsShown == 3)
        {
            esc.SetActive(true);
            rules.SetActive(false);
            tip.SetActive(false);

        }
        else if (tipsShown == 4)
        {
            esc.SetActive(false);
            help.SetActive(false);
        }

        ++tipsShown;
	}
}
