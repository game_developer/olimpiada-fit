﻿using UnityEngine;

public class MiniGame : MonoBehaviour
{
	[SerializeField] string game;
	[SerializeField] GameObject tip;

	void Start()
	{
	}

	public void LaunchGame()
	{
        Checkpoint.LastMiniGamePlayed = this.transform.GetSiblingIndex();
		UnityEngine.SceneManagement.SceneManager.LoadScene(game);
	}

	public void ToggleTip(bool show)
	{
		tip.SetActive(show);
	}
}
