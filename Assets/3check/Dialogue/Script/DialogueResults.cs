﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DialogueResults : MonoBehaviour
{
	[SerializeField] GameObject results;
	[SerializeField] QuestionAnswearManager manager;
	[SerializeField] GameObject sport;
	[SerializeField] GameObject education;
	[SerializeField] GameObject spec;
	[SerializeField] GameObject angl;
	[SerializeField] GameObject comunicate;

	public void CalculateSkils()
	{
		int sportRate = 0;
		int educationRate = 0;
		int specRate = 0;
		int angl = 0;
		int comunicateRate = 0;

		for (int i = 0; i < 50; i++)
		{
			int rate = manager.FindRate(i);
			if (i < 10)
			{
				sportRate += rate;
			}
			else if (i < 19)
			{
				educationRate += rate;
			}
			else if (i < 32)
			{
				specRate += rate;
			}
			else if (i == 32)
			{
				angl = rate;
			}
            else if (i <= 41 )
            {
                comunicateRate += rate;
            }
		}
		//HardCode(
		if (sportRate < 8)
			sport.GetComponent<TextMeshProUGUI>().text += sport.GetComponent<ResultVariants>().var_1;
		else if (sportRate <= 11)
			sport.GetComponent<TextMeshProUGUI>().text += sport.GetComponent<ResultVariants>().var_2;
		else
			sport.GetComponent<TextMeshProUGUI>().text += sport.GetComponent<ResultVariants>().var_3;
		if (educationRate < 6)
			education.GetComponent<TextMeshProUGUI>().text += education.GetComponent<ResultVariants>().var_1;
		else if (sportRate < 11)
			education.GetComponent<TextMeshProUGUI>().text += education.GetComponent<ResultVariants>().var_2;
		else
			education.GetComponent<TextMeshProUGUI>().text += education.GetComponent<ResultVariants>().var_3;
		if (specRate < 0)
			spec.GetComponent<TextMeshProUGUI>().text += spec.GetComponent<ResultVariants>().var_1;
		else if (sportRate < 20)
			spec.GetComponent<TextMeshProUGUI>().text += spec.GetComponent<ResultVariants>().var_2;
		else
			spec.GetComponent<TextMeshProUGUI>().text += spec.GetComponent<ResultVariants>().var_3;
		if (angl == 0)
			this.angl.GetComponent<TextMeshProUGUI>().text += this.angl.GetComponent<ResultVariants>().var_1;
		else
			this.angl.GetComponent<TextMeshProUGUI>().text += this.angl.GetComponent<ResultVariants>().var_2;
		if (comunicateRate < 5)
			comunicate.GetComponent<TextMeshProUGUI>().text += comunicate.GetComponent<ResultVariants>().var_1;
		else if (comunicateRate < 10)
			comunicate.GetComponent<TextMeshProUGUI>().text += comunicate.GetComponent<ResultVariants>().var_2;
		else
			comunicate.GetComponent<TextMeshProUGUI>().text += comunicate.GetComponent<ResultVariants>().var_3;
		results.SetActive(true);
	}

	public void GoToMenu()
	{
        if(StepMenu.LastUnlocked < 3)
		    StepMenu.LastUnlocked = 3;
		UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
	}
}
