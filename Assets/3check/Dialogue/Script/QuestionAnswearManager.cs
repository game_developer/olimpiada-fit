﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class QuestionAnswearManager : MonoBehaviour
{
	public static bool complited = false;
	public string[] questions = new string[60];

	public GameObject questionPattern;
	public GameObject answearPattern;
	public GameObject parent;
	public GameObject afterDialogue;
	static GameObject[] messages;

	static int[] answers = new int[60];
	static List<Vector3> positions;
	static Vector3 startQuestionPosition;
	static Vector3 startAnswearPosition;
	public static int qurentAnswear;
	static int indexMsg = 1; // for messeges array
	public static int index_question = 1; // for variants array

	void Start()
	{
		complited = StepMenu.LastUnlocked >= 3;
		if (complited)
		{
			GetComponent<DialogueResults>().CalculateSkils();
			GameObject.Find("QuestionFirst").SetActive(false);
			afterDialogue.SetActive(true);
		}
		else if (HelpDialogue.FirstTime)
		{
			messages = new GameObject[120];
			messages[0] = GameObject.Find("QuestionFirst");
			startQuestionPosition = GameObject.Find("QuestionFirst").transform.position;
			startAnswearPosition = GameObject.Find("Answear").transform.position;
			positions = new List<Vector3>();
			positions.Add(messages[0].transform.position);
		}
		// случай когда пользователь вышлел в меню и потом зашел в диалог
		else
		{
			messages[0] = GameObject.Find("QuestionFirst");
			for (int i = 1, j = 0; i < indexMsg; i++)
			{
				if (i % 2 == 1)
				{
					GameObject newMessage = Instantiate(answearPattern);
					newMessage.transform.SetParent(parent.transform, false);
					newMessage.GetComponentInChildren<Text>().text = ChooseVariant.FindVariants(answers[j])[j];
					messages[i] = newMessage;
					j++;
				}
				else
				{
					GameObject newMessage = Instantiate(questionPattern);
					newMessage.transform.SetParent(parent.transform, false);
					newMessage.GetComponentInChildren<Text>().text = questions[j];
					messages[i] = newMessage;
				}
			}
		}
		GameObject.Find("Answear").SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
		for (int i = 0; i < 120; i++)
		{
			if (messages != null && messages[i] != null)
				messages[i].transform.position += new Vector3(0, positions[i].y - messages[i].transform.position.y, 0);
		}

	}
	void NextQuestion()
	{
		for (int i = 0; i < positions.Count; i++)
		{
			positions[i] += new Vector3(0, 1.7f, 0);
		}
		GameObject newMessage = Instantiate(questionPattern);
		newMessage.transform.SetParent(parent.transform, false);
		//Запись ответа пользователя
		answers[index_question - 1] = qurentAnswear;
		newMessage.GetComponentInChildren<Text>().text = questions[index_question++];

		messages[indexMsg++] = newMessage;
		positions.Add(startQuestionPosition);
		foreach (var item in gameObject.GetComponentsInChildren<ChooseVariant>())
		{
			item.Next();
		}
	}

	void NextAnswear()
	{
		GameObject newMessage = Instantiate(answearPattern);
		newMessage.transform.SetParent(parent.transform, false);
		messages[indexMsg++] = newMessage;
		positions.Add(startAnswearPosition);
		newMessage.GetComponentInChildren<Text>().text = ChooseVariant.FindVariants(qurentAnswear)[index_question - 1];
	}

	void NextStep()
	{
		if (StepMenu.LastUnlocked < 3)
			StepMenu.LastUnlocked = 3;
		UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
	}

	public void GiveNextMessege()
	{
		DisableButton();
		for (int i = 0; i < positions.Count; i++)
		{
			positions[i] += new Vector3(0, 1.7f, 0);
		}
		NextAnswear();
		//GameObject.Find("Submit_1").GetComponent<Button>().enabled = false;
		if (index_question == 60)
		{
			complited = true;
			GetComponent<DialogueResults>().CalculateSkils();
			foreach (var item in messages)
			{
				item.SetActive(false);
			}
			foreach (var item in gameObject.GetComponentsInChildren<ChooseVariant>())
			{
				item.Next();
			}
			afterDialogue.SetActive(true);
			return;
		}
		Invoke("NextQuestion", 1);
	}
	//Делает кнопки выбора вариантов неактивными 
	void DisableButton()
	{
		foreach (var item in gameObject.GetComponentsInChildren<Button>())
		{
			item.enabled = false;
		}

	}

	public int FindRate(int questionIndex)
	{
		foreach (ChooseVariant variant in GetComponentsInChildren<ChooseVariant>())
		{
			if (variant.varIndex == answers[questionIndex])
			{
				return variant.rates[questionIndex];
			}
		}
		return -1;
	}
}
