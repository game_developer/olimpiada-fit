﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseVariant : MonoBehaviour
{
    public int varIndex;
    public string[] variants;
    public int[] rates;
    int i = 1;
    void Start()
    {
        if(!QuestionAnswearManager.complited)
        {
            GetComponentInChildren<Text>().text = variants[0];
            // случай когда пользователь вышлел в меню и потом зашел в диалог
            if (!HelpDialogue.FirstTime)
            {
                i = QuestionAnswearManager.index_question;
                GetComponentInChildren<Text>().text = variants[i - 1];
            }
        } 
        
    }
    public void Next()
    {
        if (variants[i] == "")
        {
            GetComponent<Button>().enabled = false;
        }
        else
        {
            GetComponent<Button>().enabled = true;
        }
        GetComponentInChildren<Text>().text = variants[i++];

    }
    public void Click()
    {
        QuestionAnswearManager.qurentAnswear = varIndex;
    }
    // аргумент - индекс множества ответов
    public static string[] FindVariants(int index)
    {
        ChooseVariant[] buttons = GameObject.Find("Canvas").GetComponentsInChildren<ChooseVariant>();
        foreach (var item in buttons)
        {
            if (item.varIndex == index)
                return item.variants;
        }
        return null;
    }
}
